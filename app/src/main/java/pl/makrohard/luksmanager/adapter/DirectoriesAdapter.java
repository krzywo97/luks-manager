package pl.makrohard.luksmanager.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import pl.makrohard.luksmanager.R;
import pl.makrohard.luksmanager.callback.BrowserInterface;

public class DirectoriesAdapter extends RecyclerView.Adapter<DirectoriesAdapter.DirectoriesViewHolder> {
    private Context context;
    private List<String> list;

    public DirectoriesAdapter(Context context, List<String> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public DirectoriesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.directory_item, parent, false);
        DirectoriesViewHolder viewHolder = new DirectoriesViewHolder(view);
        viewHolder.container = view.findViewById(R.id.directory_item_container);
        viewHolder.name = view.findViewById(R.id.directory_item_name);
        viewHolder.icon = view.findViewById(R.id.directory_item_icon);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final DirectoriesViewHolder holder, int position) {
        if(position > 0) {
            Glide.with(context).load(context.getDrawable(R.drawable.folder)).into(holder.icon);
        } else {
            Glide.with(context).load(context.getDrawable(R.drawable.up)).into(holder.icon);
        }
        holder.name.setText(list.get(position));

        if(context instanceof BrowserInterface) {
            holder.container.setOnClickListener(v -> {
                if(holder.getAdapterPosition() == 0) {
                    ((BrowserInterface) context).onGoUp();
                } else {
                    ((BrowserInterface) context).onDirectorySelected(list.get(holder.getAdapterPosition()));
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class DirectoriesViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView icon;
        LinearLayout container;

        DirectoriesViewHolder(View itemView) {
            super(itemView);
        }
    }
}
