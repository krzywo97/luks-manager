package pl.makrohard.luksmanager.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import pl.makrohard.luksmanager.R;
import pl.makrohard.luksmanager.callback.MountsInterface;
import pl.makrohard.luksmanager.model.MountPoint;

public class VolumesAdapter extends RecyclerView.Adapter<VolumesAdapter.VolumeViewHolder> {
    private Context context;
    private List<MountPoint> list;

    public VolumesAdapter(Context context, List<MountPoint> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public VolumeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.mount_item, parent, false);
        return new VolumeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final VolumeViewHolder holder, int position) {
        holder.path.setText(list.get(holder.getAdapterPosition()).getPath());
        holder.unmount.setOnClickListener(v -> {
            if(context instanceof MountsInterface) {
                ((MountsInterface) context).onUnmount(holder.getAdapterPosition());
            }
        });
        holder.container.setOnClickListener(v -> {
            Uri uri = Uri.parse(list.get(holder.getAdapterPosition()).getPath());
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(uri, "resource/folder");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            if(intent.resolveActivityInfo(context.getPackageManager(), 0) != null) {
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class VolumeViewHolder extends RecyclerView.ViewHolder {
        TextView path;
        ImageButton unmount;
        View container;

        VolumeViewHolder(View itemView) {
            super(itemView);
            container = itemView.findViewById(R.id.mount_item_container);
            path = itemView.findViewById(R.id.mount_item_path);
            unmount = itemView.findViewById(R.id.mount_item_unmount);
        }
    }
}
