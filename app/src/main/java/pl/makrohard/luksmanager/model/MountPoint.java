package pl.makrohard.luksmanager.model;

public class MountPoint {
    private String device, path;

    public MountPoint(String device, String path) {
        this.device = device;
        this.path = path;
    }

    public String getDevice() {
        return device;
    }

    public String getPath() {
        return path;
    }
}
