package pl.makrohard.luksmanager.fragment;

import android.support.v7.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import pl.makrohard.luksmanager.R;
import pl.makrohard.luksmanager.adapter.VolumesAdapter;
import pl.makrohard.luksmanager.callback.ActivityInterface;
import pl.makrohard.luksmanager.callback.MountsInterface;
import pl.makrohard.luksmanager.model.MountPoint;
import pl.makrohard.luksmanager.util.PredefinedDialog;

public class MainFragment extends Fragment implements MountsInterface {
    private List<MountPoint> mountPoints = new ArrayList<>();
    private ActivityInterface activityInterface;

    private AlertDialog unmountingDialog;

    private RecyclerView recyclerView;
    private View recyclerContainer, recyclerPlaceholder;

    public MainFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        initUi(view);
        initDialogs();
        getOpenVolumes();
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ActivityInterface) {
            activityInterface = (ActivityInterface) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement activityInterface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityInterface = null;
    }

    private void getOpenVolumes() {
        new Thread(() -> {
            try {
                String[] cmd = {"su", "-c", "ls -C1 /dev/mapper"};
                Process process = new ProcessBuilder(cmd).redirectErrorStream(true).start();
                BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
                String line;
                while ((line = br.readLine()) != null) {
                    if (line.startsWith("luks_open_")) {
                        mountPoints.clear();
                        String[] mountCmd = {"su", "-c", "df -h | grep \"\\/dev\\/mapper\\/luks_open_\".*"};
                        Process mountProcess = new ProcessBuilder(mountCmd).redirectErrorStream(true).start();
                        process.waitFor();
                        BufferedReader mBr = new BufferedReader(new InputStreamReader(mountProcess.getInputStream()));
                        String mountLine;
                        while ((mountLine = mBr.readLine()) != null) {
                            int tab = mountLine.lastIndexOf(" /");
                            String path = mountLine.substring(tab + 1, mountLine.length());
                            mountPoints.add(new MountPoint(line, path));

                        }
                    }
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            } finally {
                inflateRecyclerView();
            }
        }).start();
    }

    private void inflateRecyclerView() {
        if (getActivity() == null) return;
        getActivity().runOnUiThread(() -> {
            if (getContext() == null) return;
            if (mountPoints.size() > 0) {
                recyclerPlaceholder.setVisibility(View.INVISIBLE);
                recyclerContainer.setVisibility(View.VISIBLE);
                VolumesAdapter adapter = new VolumesAdapter(getContext(), mountPoints);
                LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(layoutManager);
            } else {
                recyclerContainer.setVisibility(View.INVISIBLE);
                recyclerPlaceholder.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onMount(MountPoint mountPoint) {
        mountPoints.add(mountPoint);
        inflateRecyclerView();
    }

    @Override
    public void onUnmount(int index) {
        performUnmount(mountPoints.get(index), index);
    }

    private void performUnmount(final MountPoint mountPoint, int index) {
        PredefinedDialog.show(getContext(), unmountingDialog);
        new Thread(() -> {
            try {
                String[] cmd = {"su", "-c", "umount " + mountPoint.getPath()};
                Process process = new ProcessBuilder(cmd).redirectErrorStream(true).start();
                process.waitFor();
                cmd[2] = "cryptsetup luksClose " + mountPoint.getDevice();
                process = new ProcessBuilder(cmd).redirectErrorStream(true).start();
                process.waitFor();
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            } finally {
                mountPoints.remove(index);
                inflateRecyclerView();
                PredefinedDialog.dismiss(getContext(), unmountingDialog);
            }
        }).start();
    }

    private void initUi(View view) {
        recyclerView = view.findViewById(R.id.main_recycler);
        recyclerContainer = view.findViewById(R.id.main_recycler_container);
        recyclerPlaceholder = view.findViewById(R.id.main_mounts_placeholder_container);
        FloatingActionButton fab = view.findViewById(R.id.main_fab);
        fab.setOnClickListener(v -> activityInterface.onMountClick());
        activityInterface.setToolbarTitle(null);
        activityInterface.showBackButton(false);
    }

    private void initDialogs() {
        unmountingDialog = PredefinedDialog.unmounting(getContext());
    }
}
