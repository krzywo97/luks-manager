package pl.makrohard.luksmanager.fragment;

import android.app.Activity;
import android.support.v7.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import pl.makrohard.luksmanager.R;
import pl.makrohard.luksmanager.activity.BrowseActivity;
import pl.makrohard.luksmanager.callback.ActivityInterface;
import pl.makrohard.luksmanager.callback.MountsInterface;
import pl.makrohard.luksmanager.model.MountPoint;
import pl.makrohard.luksmanager.util.PredefinedDialog;

public class MountFragment extends Fragment {
    private static final int REQUEST_GET_DIRECTORY = 1;

    private ActivityInterface activityInterface;
    private MountsInterface mountsInterface;

    private AlertDialog fetchingDialog, noVolumesFoundDialog, mountSuccessfulDialog, mountingDialog;

    private List<String> devices = new ArrayList<>();
    private Spinner devicesSpinner;
    private TextView destination, password;
    private CheckBox readOnly;

    private boolean devicesExist;

    public MountFragment() {
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mount, container, false);
        initUi(view);
        initDialogs();
        fetchLuksVolumes();
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_GET_DIRECTORY && resultCode == Activity.RESULT_OK) {
            if(data.hasExtra("path")) {
                destination.setText(data.getStringExtra("path"));
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ActivityInterface) {
            activityInterface = (ActivityInterface) context;
        } else {
            throw new RuntimeException("context should implement ActivityInterface");
        }
        if(context instanceof MountsInterface) {
            mountsInterface = (MountsInterface) context;
        } else {
            throw new RuntimeException("context should implement MountsInterface");
        }
    }

    @Override
    public void onDetach() {
        activityInterface = null;
        mountsInterface = null;
        super.onDetach();
    }

    private void fetchLuksVolumes() {
        PredefinedDialog.show(getContext(), fetchingDialog);

        new Thread(() -> {
            try {
                devices.clear();
                String[] cmd = {"su", "-c", "blkid -t TYPE=crypt_LUKS"};
                Process process = new ProcessBuilder(cmd).redirectErrorStream(true).start();
                InputStreamReader is = new InputStreamReader(process.getInputStream());
                BufferedReader br = new BufferedReader(is);
                String line;
                devicesExist = false;
                while ((line = br.readLine()) != null) {
                    if(line.length() > 1) {
                        int colon = line.indexOf(":");
                        line = line.substring(0, colon);
                        devices.add(line);
                        devicesExist = true;
                    }
                }
                if(devices.size() == 0) {
                    devices.add(getString(R.string.no_volumes_found));
                }
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (getContext() != null) {
                    ((Activity) getContext()).runOnUiThread(this::updateVolumesList);
                }
            }
        }).start();
    }

    private void updateVolumesList() {
        if(getContext() != null) {
            BaseAdapter adapter = new ArrayAdapter<>(getContext(), R.layout.device_item, devices);
            devicesSpinner.setAdapter(adapter);
            PredefinedDialog.dismiss(getContext(), fetchingDialog);
        }
    }

    private void mount(final String device, final String path, final String password, final boolean readOnly) {
        PredefinedDialog.show(getContext(), mountingDialog);

        new Thread(() -> {
            Looper.prepare();
            String[] cmd = {"su", "-c", "blkid -s UUID -o value " + device};
            try {
                Process process = new ProcessBuilder(cmd).redirectErrorStream(true).start();
                BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
                String uuid = "luks_open_" + br.readLine();

                cmd[2] = "cryptsetup luksOpen";
                if(readOnly) {
                    cmd[2] += " --readonly";
                }
                cmd[2] += " " + device + " " + uuid;
                process = new ProcessBuilder(cmd).redirectErrorStream(true).start();
                process.getOutputStream().write((password + "\n\n\n").getBytes());
                process.getOutputStream().flush();

                process.waitFor();
                int exitCode = process.exitValue();
                if(exitCode == 0) {
                    cmd[2] = "mount /dev/mapper/" + uuid + " \"" + path + "\"";
                    process = new ProcessBuilder(cmd).redirectErrorStream(true).start();
                    process.waitFor();
                    if(process.exitValue() == 0) {
                        showSuccessDialog(path);
                        if(getContext() != null) {
                            mountsInterface.onMount(new MountPoint(device, path));
                            activityInterface.popBackStack();
                        }
                    } else {
                        handleMountError(exitCode);
                    }
                } else {
                    handleOpenError(exitCode);
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            } finally {
                PredefinedDialog.dismiss(getContext(), mountingDialog);
            }
        }).start();
    }

    private void showSuccessDialog(final String name) {
        if(getActivity() == null) return;
        String message = getString(R.string.mount_successful);
        message = message.replace("%s", name);
        mountSuccessfulDialog.setMessage(message);
        PredefinedDialog.show(getContext(), mountSuccessfulDialog);
    }

    private void handleMountError(final int code) {
        if(getContext() == null || getActivity() == null) return;
        getActivity().runOnUiThread(() -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle(getString(R.string.failed_to_mount));
            String message;
            switch (code) {
                case 1:
                    message = getString(R.string.insufficient_permissions);
                    break;
                case 2:
                    message = getString(R.string.system_error);
                    break;
                case 4:
                    message = getString(R.string.missing_support);
                    break;
                default:
                    message = getString(R.string.unknown_error);
                    break;
            }
            builder.setMessage(message);
            builder.setNeutralButton(getString(R.string.ok), null);
            builder.create().show();
        });
    }

    private void handleOpenError(final int code) {
        if(getContext() == null || getActivity() == null) return;
        getActivity().runOnUiThread(() -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle(getString(R.string.failed_to_open));
            String message;
            switch (code) {
                case 2:
                    message = getString(R.string.incorrect_password);
                    break;
                default:
                    message = getString(R.string.unknown_error);
                    break;
            }
            builder.setMessage(message);
            builder.setNeutralButton(getString(R.string.ok), null);
            builder.create().show();
        });
    }

    private void initUi(View view) {
        if(getContext() == null) return;

        activityInterface.setToolbarTitle(getString(R.string.mount_volume));
        activityInterface.showBackButton(true);

        devicesSpinner = view.findViewById(R.id.mount_devices_spinner);
        destination = view.findViewById(R.id.mount_path);
        password = view.findViewById(R.id.mount_password);
        ImageButton browse = view.findViewById(R.id.mount_browse);
        readOnly = view.findViewById(R.id.mount_read_only);
        FloatingActionButton fab = view.findViewById(R.id.mount_fab);

        browse.setOnClickListener(v -> {
            if(getActivity() != null) {
                Intent browse1 = new Intent(getContext(), BrowseActivity.class);
                startActivityForResult(browse1, REQUEST_GET_DIRECTORY);
            }
        });

        fab.setOnClickListener(v -> {
            if(devicesExist) {
                String dev = (String) devicesSpinner.getSelectedItem();
                String path = destination.getText().toString();
                String pw = password.getText().toString();
                boolean ro = readOnly.isChecked();
                mount(dev, path, pw, ro);
            } else {
                PredefinedDialog.show(getContext(), noVolumesFoundDialog);
            }
        });
    }

    private void initDialogs() {
        fetchingDialog = PredefinedDialog.volumesFetcher(getContext());
        noVolumesFoundDialog = PredefinedDialog.noVolumesFound(getContext());
        mountSuccessfulDialog = PredefinedDialog.mountSuccessful(getContext());
        mountingDialog = PredefinedDialog.mounting(getContext());
    }
}
