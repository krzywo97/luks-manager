package pl.makrohard.luksmanager.callback;

public interface BinariesInterface {
    void checkRootAccess();
    void installBinary();
}
