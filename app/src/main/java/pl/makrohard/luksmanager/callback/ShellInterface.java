package pl.makrohard.luksmanager.callback;

import java.io.OutputStream;

public interface ShellInterface {
    void onProcessExit(int requestCode, int result);
    void onProcessOutput(int requestCode, String output, OutputStream input);
}
