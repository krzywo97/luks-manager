package pl.makrohard.luksmanager.callback;

import pl.makrohard.luksmanager.model.MountPoint;

public interface ActivityInterface {
    void onMountClick();
    void showBackButton(boolean show);
    void setToolbarTitle(String title);
    void popBackStack();
}
