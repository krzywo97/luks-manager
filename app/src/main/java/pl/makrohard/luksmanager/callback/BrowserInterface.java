package pl.makrohard.luksmanager.callback;

public interface BrowserInterface {
    void onGoUp();
    void onDirectorySelected(String name);
    void onDirectoryCreated(String name);
}