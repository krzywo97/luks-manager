package pl.makrohard.luksmanager.callback;

import pl.makrohard.luksmanager.model.MountPoint;

public interface MountsInterface {
    void onMount(MountPoint mountPoint);
    void onUnmount(int index);
}
