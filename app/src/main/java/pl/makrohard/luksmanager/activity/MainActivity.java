package pl.makrohard.luksmanager.activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import java.io.OutputStream;

import pl.makrohard.luksmanager.R;
import pl.makrohard.luksmanager.callback.ActivityInterface;
import pl.makrohard.luksmanager.callback.MountsInterface;
import pl.makrohard.luksmanager.callback.BinariesInterface;
import pl.makrohard.luksmanager.callback.ShellInterface;
import pl.makrohard.luksmanager.fragment.MainFragment;
import pl.makrohard.luksmanager.fragment.MountFragment;
import pl.makrohard.luksmanager.model.MountPoint;
import pl.makrohard.luksmanager.util.PredefinedDialog;
import pl.makrohard.luksmanager.util.Shell;

public class MainActivity extends AppCompatActivity implements ActivityInterface, MountsInterface, BinariesInterface, ShellInterface {
    private static final int REQUEST_CHECK_ROOT = 1;
    private static final int REQUEST_CHECK_BUSYBOX = 2;

    private FragmentManager fragmentManager;
    private ActionBar actionBar;
    private AlertDialog rootCheckerDialog, rootCheckerResultDialog;
    private MountsInterface mountsFragmentInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initDialogs();
        initUi();
        installBinary();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkRootAccess();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                fragmentManager.popBackStack();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initUi() {
        actionBar = getSupportActionBar();
        fragmentManager = getSupportFragmentManager();
        MainFragment mainFragment = new MainFragment();
        mountsFragmentInterface = mainFragment;
        setFragment(mainFragment, false);
    }

    private void initDialogs() {
        rootCheckerDialog = PredefinedDialog.rootChecker(this);
        rootCheckerResultDialog = PredefinedDialog.rootCheckFailed(MainActivity.this, MainActivity.this);
    }

    private void setFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.main_container, fragment);
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        if (addToBackStack) transaction.addToBackStack(fragment.getTag());
        transaction.commit();
    }

    @Override
    public void onMountClick() {
        MountFragment mountFragment = new MountFragment();
        setFragment(mountFragment, true);
    }

    @Override
    public void showBackButton(boolean show) {
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(show);
            actionBar.setDisplayShowHomeEnabled(show);
        }
    }

    @Override
    public void setToolbarTitle(String title) {
        if (actionBar != null) {
            actionBar.setTitle((title == null) ? getString(R.string.app_name) : title);
        }
    }

    @Override
    public void popBackStack() {
        fragmentManager.popBackStack();
    }

    @Override
    public void onMount(MountPoint mountPoint) {
        mountsFragmentInterface.onMount(mountPoint);
    }

    @Override
    public void onUnmount(int index) {
        mountsFragmentInterface.onUnmount(index);
    }

    @Override
    public void checkRootAccess() {
        rootCheckerDialog.show();

        Shell.exec("true", REQUEST_CHECK_ROOT, this);
        /*new Thread(() -> {
            int exitCode;
            boolean failed = false;
            try {
                Looper.prepare();
                Process process = Runtime.getRuntime().exec("su -c true");
                process.waitFor();
                exitCode = process.exitValue();
                process = Runtime.getRuntime().exec("busybox true");
                process.waitFor();
                failed = exitCode != 0 || process.exitValue() != 0;
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            } finally {
                PredefinedDialog.dismiss(MainActivity.this, rootCheckerDialog);
                if(failed) {
                    PredefinedDialog.show(MainActivity.this, rootCheckerResultDialog);
                }
            }
        }).start();*/
    }

    private void checkBusybox() {
        Shell.exec("busybox true", REQUEST_CHECK_BUSYBOX, this);
    }

    public void installBinary() {

    }

    @Override
    public void onProcessExit(int requestCode, int result) {
        if (requestCode == REQUEST_CHECK_ROOT) {
            if (result == 0) {
                checkBusybox();
            } else {
                PredefinedDialog.show(this, rootCheckerResultDialog);
            }
        }

        if(requestCode == REQUEST_CHECK_BUSYBOX) {
            if(result == 0) {
                PredefinedDialog.dismiss(MainActivity.this, rootCheckerDialog);
            } else {
                PredefinedDialog.show(this, rootCheckerResultDialog);
            }
        }
    }

    @Override
    public void onProcessOutput(int requestCode, String output, OutputStream input) {

    }
}
