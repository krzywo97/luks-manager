package pl.makrohard.luksmanager.activity;

import android.app.Activity;
import android.support.v7.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import pl.makrohard.luksmanager.R;
import pl.makrohard.luksmanager.adapter.DirectoriesAdapter;
import pl.makrohard.luksmanager.callback.BrowserInterface;
import pl.makrohard.luksmanager.callback.ShellInterface;
import pl.makrohard.luksmanager.util.PredefinedDialog;
import pl.makrohard.luksmanager.util.Shell;

public class BrowseActivity extends AppCompatActivity implements BrowserInterface, ShellInterface {
    private static final int REQUEST_NEW_DIRECTORY = 1;
    private static final int REQUEST_GET_DIRECTORIES = 2;

    private AlertDialog newDirectoryDialog;

    private RecyclerView browser;
    private EditText pathEdit;

    private String path = "/";
    private List<String> directories = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.choose_a_directory));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        setResult(Activity.RESULT_CANCELED);

        initUi();
        initDialogs();
        getDirectories();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.browse_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.browse_new_folder:
                showNewDirectoryDialog();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if(path.equals("/")) {
            super.onBackPressed();
        } else {
            onGoUp();
        }
    }

    private void initUi() {
        browser = findViewById(R.id.browse_browser);
        pathEdit = findViewById(R.id.browse_path);
        pathEdit.setText(path);
        Button ok = findViewById(R.id.browse_ok);
        ok.setOnClickListener(v -> {
            Intent result = new Intent();
            result.putExtra("path", path);
            setResult(RESULT_OK, result);
            finish();
        });
    }

    private void initDialogs() {
        newDirectoryDialog = PredefinedDialog.createDirectory(this, this);
    }

    private void getDirectories() {
        directories.clear();
        directories.add(getString(R.string.go_up));
        Shell.exec("ls -C1 -d " + path + (path.length() == 1 ? "" : "/") + "*/ 2>/dev/null | sort -f", REQUEST_GET_DIRECTORIES, this);
    }

    public void setPath(final String path) {
        runOnUiThread(() -> {
            String readyPath = path;
            if (path.length() > 1) {
                if (path.charAt(1) == '/') {
                    readyPath = path.substring(1, path.length());
                }
                if (path.charAt(path.length() - 1) == '/') {
                    readyPath = path.substring(0, path.length() - 1);
                }
            }
            BrowseActivity.this.path = readyPath;
            pathEdit.setText(readyPath);
            getDirectories();
        });
    }

    @Override
    public void onGoUp() {
        if (path.length() > 0) {
            new Thread(() -> {
                int slash = path.lastIndexOf("/");
                if (slash == 0) setPath("/");
                else setPath(path.substring(0, slash));
            }).start();
        }
    }

    @Override
    public void onDirectorySelected(String name) {
        setPath(path + "/" + name);
    }

    @Override
    public void onDirectoryCreated(String name) {
        Shell.exec("mkdir " + path + "/" + name, REQUEST_NEW_DIRECTORY, this);
    }

    private void showNewDirectoryDialog() {
        PredefinedDialog.show(this, newDirectoryDialog);
    }

    @Override
    public void onProcessExit(int requestCode, int result) {
        if(requestCode == REQUEST_NEW_DIRECTORY) {
            if(result == 0) {
                getDirectories();
            }
        }

        if(requestCode == REQUEST_GET_DIRECTORIES) {
            if(result == 0) {
                DirectoriesAdapter adapter = new DirectoriesAdapter(this, directories);
                LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
                browser.setLayoutManager(layoutManager);
                browser.setAdapter(adapter);
            }
        }
    }

    @Override
    public void onProcessOutput(int requestCode, String output, OutputStream input) {
        if(requestCode == REQUEST_GET_DIRECTORIES) {
            directories.add(output.substring((path.length() > 1 ? path.length() + 1 : path.length()), output.length() - 1));
        }
    }
}
