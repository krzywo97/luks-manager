package pl.makrohard.luksmanager.util;

import android.app.Activity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;

import pl.makrohard.luksmanager.callback.ShellInterface;

public class Shell {
    public static void exec(String command, int requestId, ShellInterface shellInterface) {
        String[] cmd = {"su", "-c", command};
        new Thread(() -> {
            int exitCode = -1;
            try {
                Process process = new ProcessBuilder(cmd).redirectErrorStream(true).start();
                BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
                String line;
                while ((line = br.readLine()) != null) {
                    onProcessOutput(shellInterface, requestId, line, process.getOutputStream());
                }
                process.waitFor();
                exitCode = process.exitValue();
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            } finally {
                onProcessExit(shellInterface, requestId, exitCode);
            }
        }).start();
    }

    private static void onProcessOutput(ShellInterface shellInterface, int requestId, String output, OutputStream outputStream) {
        if(shellInterface == null) return;
        if(shellInterface instanceof Activity) {
            ((Activity) shellInterface).runOnUiThread(() -> shellInterface.onProcessOutput(requestId, output, outputStream));
        } else {
            shellInterface.onProcessOutput(requestId, output, outputStream);
        }
    }

    private static void onProcessExit(ShellInterface shellInterface, int requestId, int exitCode) {
        if(shellInterface == null) return;
        if(shellInterface instanceof Activity) {
            ((Activity) shellInterface).runOnUiThread(() -> shellInterface.onProcessExit(requestId, exitCode));
        } else {
            shellInterface.onProcessExit(requestId, exitCode);
        }
    }
}