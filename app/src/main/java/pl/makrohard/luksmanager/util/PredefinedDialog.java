package pl.makrohard.luksmanager.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.inputmethodservice.InputMethodService;
import android.os.IBinder;
import android.support.v7.app.AlertDialog;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import pl.makrohard.luksmanager.R;
import pl.makrohard.luksmanager.callback.BinariesInterface;
import pl.makrohard.luksmanager.callback.BrowserInterface;

public class PredefinedDialog {
    public static AlertDialog rootChecker(Context context) {
        if (!(context instanceof Activity)) return null;
        return new AlertDialog.Builder(context)
                .setTitle(R.string.checking_root_access)
                .setView(R.layout.progress_dialog)
                .setCancelable(false)
                .create();
    }

    public static AlertDialog rootCheckFailed(Context context, BinariesInterface tryAgainHandler) {
        if (!(context instanceof Activity)) return null;
        return new AlertDialog.Builder(context)
                .setTitle(R.string.not_rooted)
                .setMessage(R.string.not_rooted_message)
                .setPositiveButton(R.string.ok, (dialog, which) -> ((Activity) context).finish())
                .setNegativeButton(R.string.try_again, (dialog, which) -> tryAgainHandler.checkRootAccess())
                .create();
    }

    public static AlertDialog volumesFetcher(Context context) {
        if (!(context instanceof Activity)) return null;
        return new AlertDialog.Builder(context)
                .setTitle(R.string.fetching_volumes)
                .setView(R.layout.progress_dialog)
                .setCancelable(false)
                .create();
    }

    public static AlertDialog noVolumesFound(Context context) {
        if (!(context instanceof Activity)) return null;
        return new AlertDialog.Builder(context)
                .setTitle(R.string.no_volumes_found)
                .setMessage(R.string.no_volumes_found_explanation)
                .setNeutralButton(R.string.ok, null)
                .create();
    }

    public static AlertDialog mountSuccessful(Context context) {
        if (!(context instanceof Activity)) return null;
        return new AlertDialog.Builder(context)
                .setTitle(R.string.success)
                .setMessage(R.string.mount_successful)
                .setNeutralButton(R.string.ok, null)
                .create();
    }

    public static AlertDialog mounting(Context context) {
        if (!(context instanceof Activity)) return null;
        return new AlertDialog.Builder(context)
                .setTitle(R.string.mounting)
                .setView(R.layout.progress_dialog)
                .setCancelable(false)
                .create();
    }

    public static AlertDialog unmounting(Context context) {
        if (!(context instanceof Activity)) return null;
        return new AlertDialog.Builder(context)
                .setTitle(R.string.unmounting)
                .setView(R.layout.progress_dialog)
                .setCancelable(false)
                .create();
    }

    public static AlertDialog binaryNotPresent(Context context, BinariesInterface binariesInterface) {
        if (!(context instanceof Activity)) return null;
        return new AlertDialog.Builder(context)
                .setTitle(R.string.missing_binary)
                .setMessage(R.string.missing_binary_explanation)
                .setPositiveButton(R.string.ok, (dialog, which) -> binariesInterface.installBinary())
                .setNegativeButton(R.string.cancel, ((dialog, which) -> ((Activity) context).finish()))
                .create();
    }

    public static AlertDialog installingBinary(Context context) {
        if (!(context instanceof Activity)) return null;
        return new AlertDialog.Builder(context)
                .setTitle(R.string.installing_binary)
                .setView(R.layout.progress_dialog)
                .setCancelable(false)
                .create();
    }

    public static AlertDialog createDirectory(Context context, BrowserInterface browserInterface) {
        if (!(context instanceof Activity)) return null;
        final AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle(R.string.create_directory)
                .setView(R.layout.new_folder_dialog)
                .setPositiveButton(R.string.ok, null)
                .setNegativeButton(R.string.cancel, null)
                .create();

        dialog.setOnShowListener(dialog1 -> {
            Button positive = dialog.getButton(Dialog.BUTTON_POSITIVE);
            EditText name = dialog.findViewById(R.id.new_folder_name);
            if (name == null) return;
            name.setText(null);
            InputMethodManager iss = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            if(iss != null)
                iss.showSoftInput(name, InputMethodManager.SHOW_IMPLICIT);

            positive.setOnClickListener(v -> {
                String folder_name = name.getText().toString();
                ((Activity) context).runOnUiThread(() -> {
                    if (folder_name.matches("^[^\\\\/?%*:|\"<>.]+$")) {
                        browserInterface.onDirectoryCreated(folder_name);
                        name.setError(null);
                        dialog.dismiss();
                    } else {
                        name.setError(context.getString(R.string.invalid_name));
                    }
                });
            });
        });

        //TODO: fix keyboard to always hide on dialog dismiss
        dialog.setOnDismissListener((dialogInterface) -> {
            EditText name = dialog.findViewById(R.id.new_folder_name);
            if(name == null) return;
            name.requestFocus();
            InputMethodManager iss = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            View focus = ((Activity) context).getCurrentFocus();
            if(iss != null && focus != null) {
                iss.hideSoftInputFromWindow(focus.getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);
            }
        });
        return dialog;
    }

    public static void show(Context context, final AlertDialog dialog) {
        if (!(context instanceof Activity)) return;
        ((Activity) context).runOnUiThread(dialog::show);
    }

    public static void dismiss(Context context, final AlertDialog dialog) {
        if (!(context instanceof Activity)) return;
        ((Activity) context).runOnUiThread(dialog::dismiss);
    }
}
